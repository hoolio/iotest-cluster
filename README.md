# iotest-cluster
deploy [iotest](https://gitlab.com/hoolio/iotest) to cluster nodes to test disk io.

## Requirements
* Requires ansible on the deployment node.  
* All cluster nodes share a common mount for use as logging path
* NOTE: the common mount shouldn't be the same path as that being monitored
* Ssh key access required between deployment and cluster nodes

## General operation
1. this tool used to push configs to cluster nodes in accordance with Requirements
1. nodes run iotest via a cron entry and log data to shared logging path
1. data analysis is done on resulting logfiles.  BYO Data Scientist.

## Setup
1. you should change hosts to suit your environment.
1. check the crontab entry and change to suit your environment.
1. deploy to nodes as follows:
```
$ ansible-playbook -i hosts bootstrap.yml

PLAY ***************************************************************************

TASK [install required softwares (git, numpy and dd)] **************************
ok: [144.6.228.111] => (item=[u'git', u'python-numpy', u'coreutils'])
ok: [144.6.235.160] => (item=[u'git', u'python-numpy', u'coreutils'])
ok: [144.6.232.113] => (item=[u'git', u'python-numpy', u'coreutils'])
ok: [144.6.237.67] => (item=[u'git', u'python-numpy', u'coreutils'])
ok: [144.6.237.65] => (item=[u'git', u'python-numpy', u'coreutils'])
ok: [144.6.234.145] => (item=[u'git', u'python-numpy', u'coreutils'])
ok: [144.6.224.48] => (item=[u'git', u'python-numpy', u'coreutils'])

TASK [pull down the iotest github repository] **********************************
ok: [144.6.235.160]
ok: [144.6.228.111]
ok: [144.6.232.113]
ok: [144.6.237.65]
ok: [144.6.237.67]
ok: [144.6.234.145]
ok: [144.6.224.48]

TASK [perform a single operation to ensure iotest works] ***********************
changed: [144.6.235.160]
changed: [144.6.237.65]
changed: [144.6.237.67]
changed: [144.6.232.113]
changed: [144.6.224.48]
changed: [144.6.228.111]
changed: [144.6.234.145]

PLAY RECAP *********************************************************************
144.6.224.48               : ok=3    changed=1    unreachable=0    failed=0
144.6.228.111              : ok=3    changed=1    unreachable=0    failed=0
144.6.232.113              : ok=3    changed=1    unreachable=0    failed=0
144.6.234.145              : ok=3    changed=1    unreachable=0    failed=0
144.6.235.160              : ok=3    changed=1    unreachable=0    failed=0
144.6.237.65               : ok=3    changed=1    unreachable=0    failed=0
144.6.237.67               : ok=3    changed=1    unreachable=0    failed=0
```
